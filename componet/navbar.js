const navbar = () => {
  return `<div id="topnav">
    <div id="topbtn">
        <div id="usic">
            <img style="width:10px;height:10px;" src="https://cdn-icons-png.flaticon.com/128/323/323310.png" alt="">
            <p>us-USD</p>
        </div>
     </div>
</div>
<div id="middlenav">
<a href="/" style="text-wrap: wrap;font-size: xx-large;font-weight: bold;padding-left: 10px;">
LINGIANG COSMETIC
</a>

    </a>
    <div id="serachkaro">
      <div class="dropdown">
        <div id="serachkaro" class="dropbtn">
            <input id="serachnosarchiyo" type="text" placeholder="Search for a product or brand...">
            <button id="serchiyanubuuton"><i class="fa-solid fa-magnifying-glass"></i></button>
          </div>
          <div class="dropdown-content">
            <div id="serachdata"></div>
          </div>
        </div>
    </div>
    <div id="acc_cart">
        <div id="account">
            <div class="dropdown">
                <button class="dropbtn">
                    <i class="fa-regular fa-user"></i><span>Account</span>
                </button>
                <div class="dropdown-content">
                  <div id="mainacc">
                  <div id="lrmenu">
                    <button id="loginacc"><a href="./login.html">Login</a></button>
                    <button id="regtoop"><a href="./registration.html">Singup</a></button>
                    </div>
                    <div id="otheracc">
                      <div>Wishlist</div>
                      <div>Your Orders</div>
                      <div>Your Referrals</div>
                    </div>
                  </div>
                </div>
              </div> 
            
        </div>
        <div id="cartmenus"></div>
        <div class="dropdown">
            <button id="svgcart" class="dropbtn">
                <svg class="responsiveFlyoutBasket_icon responsiveFlyoutBasket_icon-basket" width="24" height="24" >
                    <path d="M6.57412994,10 L17.3932043,10 L13.37,4.18336196 L15.0021928,3 L19.8438952,10 L21,10 C21.5522847,10 22,10.4477153 22,11 C22,11.5522847 21.5522847,12 21,12 L17.5278769,19.8122769 C17.2068742,20.534533 16.4906313,21 15.7002538,21 L8.29974618,21 C7.50936875,21 6.79312576,20.534533 6.47212308,19.8122769 L3,12 C2.44771525,12 2,11.5522847 2,11 C2,10.4477153 2.44771525,10 3,10 L4.11632272,10 L9,3 L10.6274669,4.19016504 L6.57412994,10 Z M5.18999958,12 L8.29999924,19 L15.6962585,19 L18.8099995,12 L5.18999958,12 Z"></path>
                  </svg> <span id="cartt"><a href="./cart.html"><p id="cartt">cart</p></a></span>
            </button>
            <div class="dropdown-content">
               <div id="cartcontent">There are currently no items in your cart.</div>
            </div>
          </div> 

    </div>
</div>
<hr style="height: 1.5px;background-color: black;">
<div id="bottomnav">
  <div><a href="Skincare.html">Skin care</a></div>
  <div><a href="Haircare.html">Hair</a></div>
  <div><a href="Makeup.html">Makeup</a></div>
  <div><a href="Body.html">Bath & Body</a></div>
  <div><a href="AllSkincare.html">Fragrance</a></div>
  <div><a href="shop1.html">Self Care</a></div>
  <div><a href="co-operation">E-commerce Co-operation</a></div>
  <div id="salariya">Advice
    <div class="mydrop" id="saledrop">
      <div><a href="Skincare.html">Skin care</a></div>
      <div><a href="Makeup.html">Makeup</a></div>
      <div><a href="Haircare.html">Haircare</a></div>
      <div><a href="Body.html">Body</a></div>
      <div><a href="AllSkincare.html">Tip & Advice</a></div>
      <div><a href="AllSkincare.html">Beuaty Center</a></div>
      <div><a href="AllSkincare.html">Free Beauty Consulataions</a></div>
    </div>
  </div>
</div>`
}

export default navbar;